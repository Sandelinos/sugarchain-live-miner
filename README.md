# Sugarchain Live Miner

This is a debian live image that loads itself onto the ram and starts mining sugarchain without needing the USB plugged in.
This way you can mine sugarchain on any machine without making any changes to the system.

# How to compile it
Tested on Debian 10.2
```bash
sudo apt install live-build #for the live image
sudo apt install build-essential libssl-dev libcurl4-openssl-dev libjansson-dev libgmp-dev automake zlib1g-dev #for cpuminer-opt-sugarchain
git clone --recurse-submodules https://gitlab.com/Sandelinos/sugarchain-live-miner.git
cd sugarchain-live-miner
make
```
The output image will be named live-image-amd64.img

# How to use it
 1. Compile the image yourself or download a precompiled one from [this torrent link](magnet:?xt=urn:btih:50f10feb83866d94f61bdec29bf2c4434489674a&dn=sugarchain-live-miner%5Fv1.0.img&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80)
 2. Write it onto a USB stick using dd.(etcher and other tools should work too but I haven't tested them)
```bash
sudo dd if=live-image-amd64.img of=/dev/your_usb_stick bs=4M conv=fsync
```
 3. Edit the file SUGAR.json in the root of your USB stick and set settings as you like
 4. Plug the stick into a computer and boot it and watch the magic happen.
 5. Unplug the stick if you want.
 *  The live user's password is "live"
 *  To boot the PC back into it's normal OS just run `sudo reboot`
 *  The miner runs as a systemd service "sugarminer.service", to check it's status run `systemctl status sugarminer` or `sudo journalctl -u sugarminer` for the output from the miner program itself.
 *  You can check CPU temperature with the command `sensors` or interactively monitor it with `watch -n1 sensors` (this might not work with every computer)

## Todo:

 - [x] Make a hook to compile cpuminer-opt-sugarchain from source when building the image
 - [x] Compile a .img instead of .iso so the configuration can be edited after writing the USB.
 - [ ] Fix booting in UEFI mode.


